import { ViewUsersComponent } from './users/components/view-users/view-users.component';
import { EditUsersComponent } from './users/components/edit-users/edit-users.component';
import { AuthGuard } from './auth/auth.guard';
import { AddBooksComponent } from './books/components/add-books/add-books.component';
import { AddUsersComponent } from './users/components/add-users/add-users.component';
import { DashboardComponent } from './users/components/dashboard/dashboard.component';
import { LoginComponent } from './auth/components/login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{RouterModule,Routes} from '@angular/router';


const routes:Routes=[
  {
    path:'login',
    component:LoginComponent
     },
  {
  path:'dashboard',
  component:DashboardComponent,
  canActivate:[AuthGuard]
   },
   {
    path:'edit/:id',
    component:EditUsersComponent
     },
        {
   path:'viewusers',
     component:ViewUsersComponent
      },

  //  {
  //   path:'addusers',
  //   component:AddUsersComponent
  //    },
  //    {
  //     path:'addbooks',
  //     component:AddBooksComponent
  //      },
  {
    path:'',
    redirectTo:'/login',
    pathMatch:'full'
  },
  
]


@NgModule({
  imports: [
    CommonModule,
     RouterModule.forRoot(routes),
  ],
  exports:[
     RouterModule
  ],
  declarations: []
})
export class ApproutingModule { }

