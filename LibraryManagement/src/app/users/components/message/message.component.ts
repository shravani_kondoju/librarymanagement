import { Subject } from 'rxjs';
import { AddserviceService } from './../../../addservice.service';
import { AuthService } from './../../../auth/auth.service';
import { MessageService } from './../../../message.service';
import { ViewBooksComponent } from './../../../books/components/view-books/view-books.component';
import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  public approvechangeEvent : Subject<boolean> = new Subject<boolean>();
  approv=false;
request:any
  constructor(private msg:MessageService ,private auth:AuthService, private addservice:AddserviceService) { }
arr;
admin;
message;
// email:any;
  ngOnInit() {
  // this.arr=this.msg.getcurData()[0].concat(this.msg.getcurData()[1])
    // console.log(this.msg.getcurData()[0] );
   this.getmsg();
  //  this.approv = this.auth.getCurrentUser();
    this.approvechangeEvent.subscribe(approv =>{
    this.approv = approv;
  });

   this.admin = this.auth.getCurrentUser();
    this.auth.adminchangeEvent.subscribe(admin =>{
    this.admin = admin;
  });

  
  }
  
  getmsg() {
    this.addservice.getMsg().subscribe(res => {
    this.message = res;
    });
    }
// user=this.msg.getcurData()[1].mail;
// bookId=this.msg.getcurData()[0].bookId;
// bookName=this.msg.getcurData()[0].bookName;
// category=this.msg.getcurData()[0].category;

  approve(){
    this.approvechangeEvent.next(true);
    
  }

}
