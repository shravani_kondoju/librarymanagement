import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBooksComponent } from './components/add-books/add-books.component';
import { ViewBooksComponent } from './components/view-books/view-books.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriesComponent } from './components/categories/categories.component';
import {NgxPaginationModule} from 'ngx-pagination'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule
  ],
  exports:[
    AddBooksComponent,ViewBooksComponent
  ],
  declarations: [AddBooksComponent, ViewBooksComponent, CategoriesComponent]
})
export class BooksModule { }
