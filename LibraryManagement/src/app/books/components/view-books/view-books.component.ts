import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit, Pipe } from '@angular/core';
@Component({
  selector: 'app-view-books',
  templateUrl: './view-books.component.html',
  styleUrls: ['./view-books.component.css'],
})
export class ViewBooksComponent implements OnInit {
books:any;
cates:any;
op:any;
book:any;
  constructor(private service:AddserviceService) { }

  ngOnInit() {
    this.getbook();
    this.viewcate();
    this.book=this.books;
  }
  getbook(){
    this.service.getBooks().subscribe(res => {
      this.books = res;
    });

  }
  viewcate()
  {
    this.service.getCate().subscribe(res => {
      this.cates = res;
    });
  }
  // getOption(option)
  // {
  //   this.op=option;
  // }
  filterForeCasts(filterVal: any) {
    if(this.books.categoryName != filterVal)
    {
      this.book=null;
    }
    if (filterVal == "All")
    {
      this.book=this.books;
    }
     
    else
    {
      this.book = this.books.filter((item) => item.categoryName.indexOf(filterVal) !== -1);
    }    
}
q: number = 1;

}
