import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-books',
  templateUrl: './add-books.component.html',
  styleUrls: ['./add-books.component.css']
})
export class AddBooksComponent implements OnInit {
  
  title = 'Add Books';
   angForm: FormGroup;
   cates:any;
  constructor(private addservice: AddserviceService, private fb: FormBuilder, private router: Router) {
    this.createForm();
       }
         createForm() {
    this.angForm = this.fb.group({
      bookId: ['', Validators.required ],
      categoryName: ['', Validators.required ],
      author: ['', Validators.required ],
      publ: ['', Validators.required ],
      count: ['', Validators.required ],

      bookName: ['', Validators.required ]
   });
  }
  addBook(bookID, bookName, category, author, publ, count) {
      this.addservice.addBook(bookID, bookName, category, author, publ, count);
      console.log(bookID);
  }
  getcate(){
    this.addservice.getCate().subscribe(res => {
      this.cates = res;
    });
  }

  ngOnInit() {
    this.getcate();
  }

}
