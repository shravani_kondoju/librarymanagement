var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Items


var Books = new Schema({
  bookId: {
    type: Number
  },
  bookName: {
    type: String
  },
  categoryName: {
    type: String
  },
  author: {
    type: String
  },
  publ: {
    type: String
  },
  count: {
    type: Number
  }
  
},{
    collection: 'Books'
});
 module.exports = mongoose.model('Books', Books);
